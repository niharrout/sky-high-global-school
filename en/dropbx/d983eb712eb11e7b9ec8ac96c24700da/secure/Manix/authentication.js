// JavaScript Document

function showHelp(param) {
		var width=390;
		var height=400;
		var top=(screen.height-height)/2;
		var left=(screen.width-width)/2;
		//var chaine = "top="+top+",left="+left+",width="+width+",height="+height;
		var chaine = "top="+0+",left="+0+",width="+width+",height="+height;

    		  window.open("./help.jsp;jsessionid="+param, "", chaine);
	}

function init(){
	//document.getElementById('btn_ok').disabled = true;
}

function center()
 {
	var width=390;
	var height=400;
  	var object=document.getElementById("allcenter");
	object.style.marginLeft = "-" + parseInt(object.offsetWidth / 2) + "px";
  	object.style.marginTop = "-" +parseInt(object.offsetHeight / 2) + "px";
 }

function init_foc(champ){
	var Obj = document.getElementById(champ);
	Obj.value = "";
	var btn_ok = document.getElementById('btn_ok');
	var oldSrc = btn_ok.src.substring(0, btn_ok.src.length-7);
	var newSrc = oldSrc.concat('off.png');
	btn_ok.src = newSrc;
	btn_ok.disabled = true;
}

function controle_date(champ){
	var Obj = document.getElementById(champ);
	if(Obj.value.length == 2 && champ != "chp3"){
		var chp = champ.substring(3,4);
		chp++;
		var newId = "chp"+chp;
		var newObj = document.getElementById(newId);
		newObj.focus();
	}else if(Obj.value.length == 4 && document.getElementById('chp1').value.length == 2 && document.getElementById('chp2').value.length == 2){
		var chp3 = document.getElementById('chp3');
		chp3.blur();
		var btn_ok = document.getElementById('btn_ok');
		var oldSrc = btn_ok.src.substring(0, btn_ok.src.length-7);
		var newSrc = oldSrc.concat('onn.png');
		btn_ok.disabled = false;
		btn_ok.src = newSrc;
	}
}


function controle_zipcode(champ){
	var Obj = document.getElementById(champ);
	if(Obj.value.length == 5){
		var btn_ok = document.getElementById('btn_ok');
		var oldSrc = btn_ok.src.substring(0, btn_ok.src.length-7);
		var newSrc = oldSrc.concat('onn.png');
		btn_ok.disabled = false;
		btn_ok.src = newSrc;
	} else {
		var btn_ok = document.getElementById('btn_ok');
		var oldSrc = btn_ok.src.substring(0, btn_ok.src.length-7);
		var newSrc = oldSrc.concat('off.png');
		btn_ok.src = newSrc;
		btn_ok.disabled = true;
	}
}

function controle_sms(champ){
	var Obj = document.getElementById(champ);
	if(Obj.value.length == 8){
		var btn_ok = document.getElementById('btn_ok');
		var oldSrc = btn_ok.src.substring(0, btn_ok.src.length-7);
		var newSrc = oldSrc.concat('onn.png');
		btn_ok.disabled = false;
		btn_ok.src = newSrc;
	} else {
		var btn_ok = document.getElementById('btn_ok');
		var oldSrc = btn_ok.src.substring(0, btn_ok.src.length-7);
		var newSrc = oldSrc.concat('off.png');
		btn_ok.src = newSrc;
		btn_ok.disabled = true;
	}
}

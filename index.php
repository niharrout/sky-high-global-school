<?php include 'filesLogic.php';?>


<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>



<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="Sky-high-global-school" />
<meta name="keywords" content="sky high global school, school in Bhubaneshwar, SHG School, Wiselab" />
<meta name="author" content="Nihar Ranjan Rout" />

<!-- Page Title -->
<title>Sky High Global School | Best Play School In Bhubaneswar </title>

<!-- Favicon and Touch Icons -->
<link href="images/favicon.png" rel="shortcut icon" type="image/png">
<link href="images/apple-touch-icon.png" rel="apple-touch-icon">
<link href="images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
<link href="images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
<link href="images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="css/menuzord-skins/menuzord-rounded-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<!-- CSS | Custom Margin Padding Collection -->
<link href="css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- Revolution Slider 5.x CSS settings -->
<link  href="js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link  href="js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
<link  href="js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>

<!-- CSS | Theme Color -->
<link href="css/colors/theme-skin-color-set-1.css" rel="stylesheet" type="text/css">

<!-- external javascripts -->
<script src="js/jquery-2.2.4.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="js/jquery-plugin-collection.js"></script>

<!-- Revolution Slider 5.x SCRIPTS -->
<script src="js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->





</head>
<body class="">
<div id="wrapper" class="clearfix">

  
  <!-- Header -->
  <header id="header" class="header">
    <div class="header-top bg-theme-color-sky sm-text-center p-0">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="widget no-border m-0">
              <ul class="list-inline font-13 sm-text-center mt-5">
                <li>
                  <a class="text-white" href="faqs.html">FAQ</a>
                </li>
                <li class="text-white">|</li>
                <li>
                  <a class="text-white" href="contact-us.html">Help Desk</a>
                </li>
                <li class="text-white">|</li>
                <li>
                  <a class="text-white" href="insider/index.php">Login</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-8">
            <div class="widget m-0 pull-right sm-pull-none sm-text-center">
              <ul class="list-inline pull-right">
                <li class="mb-0 pb-0">
                  <div class="top-dropdown-outer pt-5 pb-10">
                    <a class="top-cart-link has-dropdown text-white text-hover-theme-colored"></a>
                    <ul class="dropdown">
                      <li>
                        <!-- dropdown cart -->
                        <div class="dropdown-cart">
                          <table class="table cart-table-list table-responsive">
                            
                          </table>
                          <div class="total-cart text-right">
                            <table class="table table-responsive">
                              
                            </table>
                          </div>
                         
                        </div>
                        <!-- dropdown cart ends -->
                      </li>
                    </ul>
                  </div>
                </li>
               
              </ul>
            </div>
            <div class="widget no-border m-0 mr-15 pull-right flip sm-pull-none sm-text-center">
              <ul class="styled-icons icon-circled icon-sm pull-right flip sm-pull-none sm-text-center mt-sm-15">
                <li><a href="https://www.facebook.com/skyhighglobalschool"><i class="fa fa-facebook text-white"></i></a></li>
              
               
                <li><a href="https://www.instagram.com/skyhighglobalbbsr/"><i class="fa fa-instagram text-white"></i></a></li>
                
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-middle p-0 bg-lightest xs-text-center">
      <div class="container pt-0 pb-0">
        <div class="row">
          <div class="col-xs-12 col-sm-4 col-md-5">
            <div class="widget no-border m-0">
              <a class="menuzord-brand pull-left flip xs-pull-center mb-15" href="javascript:void(0)"><img src="image/logo.png" alt=""></a>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
              <ul class="list-inline">
                <li><i class="fa fa-phone-square text-theme-color-sky font-36 mt-5 sm-display-block"></i></li>
                <li>
                  <a href="#" class="font-12 text-black text-uppercase">Call us today!</a>
                  <h5 class="font-14 text-theme-color-blue m-0"> +91 7008776394</h5>
                </li>
              </ul>
            </div>
          </div>  
          <div class="col-xs-12 col-sm-4 col-md-3">
            <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
              <ul class="list-inline">
                <li><i class="fa fa-clock-o text-theme-color-red font-36 mt-5 sm-display-block"></i></li>
                <li>
                  <a href="#" class="font-12 text-black text-uppercase">We are open!</a>
                  <h5 class="font-13 text-theme-color-blue m-0"> Mon-Fri</h5>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper navbar-scrolltofixed bg-theme-color-red border-bottom-theme-color-sky-2px">
        <div class="container">
          <nav id="menuzord" class="menuzord bg-theme-colored pull-left flip menuzord-responsive">
            <ul class="menuzord-menu">
              <li class="active"><a href="index.php">Home</a>
                
              </li>
			    <li><a href="about.html">About Us</a>
                
              </li>
             
			  
			     <li><a href="#home">Methodology</a>
                <ul class="dropdown">
                  <li><a href="curriculum.html">Curriculum</a></li>
                  <li><a href="true-smart-learning-systems.html">True Smart Learning System </a></li>
                  <li><a href="facilities.html">Facilities </a></li>
                </ul>
              </li>
			  	     <li><a href="#home">Programs</a>
                <ul class="dropdown">
                  <li><a href="turbo-toddler-program.html">Turbo Toddler (play group)</a></li>
                  <li><a href="nursery-school-program.html"> Jumbo Nursery(nursery)</a></li>
                  <li><a href="lower-kg-school-program.html">Dumbo junior (LKG) </a></li>
				  
                  <li><a href="upper-kg-program.html">Tembo senior (UKG)</a></li>
               
                </ul>
              </li>
			  
			    <li><a href="day-care.html">Day Care</a>
                
              </li>
             
              <li><a href="#home">Franchise</a>
                <ul class="dropdown">
                  <li><a href="what-you-need.html">What You Need </a></li>
                  <li><a href="shgs-support.html">SHGS Support </a></li>
                  <li><a href="how-to-start.html">How To Start </a></li>
				   <li><a href="application-form.html">Application Form </a></li>
                </ul>
              </li>
			   <li><a href="gallery.html">Gallery</a>
                
              </li>  
                    <li><a href="notice.php">Notice</a>
                
              </li> 
               <li><a href="contact-us.html">Contact Us</a>
                
              </li>     
            <ul class="pull-right flip hidden-sm hidden-xs">
              <li>
                <!-- Modal: Book Now Starts -->
                <a class="btn btn-colored btn-flat bg-theme-color-sky text-white font-14 bs-modal-ajax-load mt-0 p-25 pr-15 pl-15" href="admission.html"><b>Admission Inquiry</b></a>
                <!-- Modal: Book Now End -->
              </li>
            </ul>
          
          </nav>
        </div>
      </div>
    </div>
  </header>
  
  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: home -->
    <section id="home">
      <div class="container-fluid p-0">
	  
        <div class="col-lg-8 col-md-8 col-sm-8">
        <!-- Slider Revolution Start -->
        <div class="rev_slider_wrapper">
          <div class="rev_slider rev_slider_default" data-version="5.0">
            <ul>

              <!-- SLIDE 1 -->
              <li data-index="rs-1" data-transition="fade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="images/bg/bg1.jpg" data-rotate="0"  data-fstransition="fade" data-saveperformance="off" data-title="Web Show" data-description="">
                <!-- MAIN IMAGE -->
                <img src="images/bg/bg1.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
                  id="slide-1-layer-1" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                  data-width="full"
                  data-height="full"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                  data-start="1000" 
                  data-basealign="slide" 
                  data-responsive_offset="on" 
                  style="z-index: 5;background-color:rgba(0, 0, 0, 0.05);border-color:rgba(0, 0, 0, 1.00);"> 
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme sft font-Lobster font-weight-700 text-theme-color-red" 
                  id="rs-1-layer-2"

                  data-x="['left']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['-115']"
                  data-fontsize="['28']"
                  data-lineheight="['48']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-speed="500"
                  data-start="600"
                  data-splitin="none"
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 5; white-space: nowrap;">
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption tp-resizeme sft text-theme-color-blue font-Lobster font-weight-700 m-0" 
                  id="rs-1-layer-3"

                  data-x="['left']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['-50']"
                  data-fontsize="['78']"
                  data-lineheight="['96']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-speed="500"
                  data-start="800"
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 5; white-space: nowrap;">Play,Learn <br> <span class="text-theme-color-red"> And Discover</span>
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption tp-resizeme sft text-black" 
                  id="rs-1-layer-4"

                  data-x="['left']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['20']"
                  data-fontsize="['16','18',24']"
                  data-lineheight="['28']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-speed="500"
                  data-start="1200"
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 5; white-space: nowrap; font-weight:500;">
                </div>

                <!-- LAYER NR. 5 -->
                <div class="tp-caption sft" 
                  id="rs-1-layer-5"

                  data-x="['left']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['90','100','110']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-speed="500"
                  data-start="1400"
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored btn-theme-color-blue btn-lg btn-flat font-weight-600 pl-30 pr-30" href="contact-us.html">Contact Us</a>
                </div>
              </li>

              <!-- SLIDE 2 -->
              <li data-index="rs-2" data-transition="fade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="images/bg/bg2.jpg" data-rotate="0"  data-fstransition="fade" data-saveperformance="off" data-title="Web Show" data-description="">
                <!-- MAIN IMAGE -->
                <img src="images/bg/bg2.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
                  id="slide-2-layer-1" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                  data-width="full"
                  data-height="full"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                  data-start="1000" 
                  data-basealign="slide" 
                  data-responsive_offset="on" 
                  style="z-index: 5;background-color:rgba(0, 0, 0, 0.05);border-color:rgba(0, 0, 0, 1.00);"> 
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme sft font-Lobster font-weight-700 text-theme-color-red" 
                  id="rs-2-layer-2"

                  data-x="['right']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['-115']"
                  data-fontsize="['28']"
                  data-lineheight="['48']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-speed="500"
                  data-start="600"
                  data-splitin="none"
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 5; white-space: nowrap;">
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption tp-resizeme sft text-theme-color-blue font-Lobster font-weight-700 m-0" 
                  id="rs-2-layer-3"

                  data-x="['right']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['-50']"
                  data-fontsize="['78']"
                  data-lineheight="['96']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-speed="500"
                  data-start="800"
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 5; white-space: nowrap;">unconventional ways <br><span class="text-theme-color-red">of learning.</span>
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption tp-resizeme sft text-black-333" 
                  id="rs-2-layer-4"

                  data-x="['right']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['20']"
                  data-fontsize="['16','18',24']"
                  data-lineheight="['28']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-speed="500"
                  data-start="1200"
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 5; white-space: nowrap; font-weight:500;">
                </div>

                <!-- LAYER NR. 5 -->
                <div class="tp-caption sft" 
                  id="rs-2-layer-5"

                  data-x="['right']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['90','100','110']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-speed="500"
                  data-start="1400"
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored btn-theme-color-blue btn-lg btn-flat font-weight-600 pl-30 pr-30" href="contact-us.html">Contact Us</a>
                </div>
              </li>

              <!-- SLIDE 3 -->
              <li data-index="rs-3" data-transition="fade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="images/bg/bg4.jpg" data-rotate="0"  data-fstransition="fade" data-saveperformance="off" data-title="Web Show" data-description="">
                <!-- MAIN IMAGE -->
                <img src="images/bg/bg4.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
                  id="slide-3-layer-1" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                  data-width="full"
                  data-height="full"
                  data-whitespace="normal"
                  data-transform_idle="o:1;"
                  data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
                  data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                  data-start="1000" 
                  data-basealign="slide" 
                  data-responsive_offset="on" 
                  style="z-index: 5;background-color:rgba(0, 0, 0, 0.05);border-color:rgba(0, 0, 0, 1.00);"> 
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme sft font-Lobster font-weight-700 text-theme-color-red" 
                  id="rs-3-layer-2"

                  data-x="['left']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['-115']"
                  data-fontsize="['28']"
                  data-lineheight="['48']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-speed="500"
                  data-start="600"
                  data-splitin="none"
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 5; white-space: nowrap;">
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption tp-resizeme sft text-theme-color-blue font-Lobster font-weight-700 m-0" 
                  id="rs-3-layer-3"

                  data-x="['left']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['-50']"
                  data-fontsize="['78']"
                  data-lineheight="['96']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-speed="500"
                  data-start="800"
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on" 
                  style="z-index: 5; white-space: nowrap;">Grooming  leaders <br><span class="text-theme-color-red">of the future</span>
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption tp-resizeme sft text-black" 
                  id="rs-3-layer-4"

                  data-x="['left']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['20']"
                  data-fontsize="['16','18',24']"
                  data-lineheight="['28']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-speed="500"
                  data-start="1200"
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 5; white-space: nowrap; font-weight:500;">
                </div>

                <!-- LAYER NR. 5 -->
                <div class="tp-caption sft" 
                  id="rs-3-layer-5"

                  data-x="['left']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['90','100','110']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-speed="500"
                  data-start="1400"
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored btn-theme-color-blue btn-lg btn-flat font-weight-600 pl-30 pr-30" href="contact-us.html">Contact Us</a>
                </div>
              </li>

            </ul>
			
          </div>
		  </div>
		  </div>
		  
		  <style>
		 hr{
  border: 1px solid black;
}
		  </style>
          <!-- end .rev_slider -->
		  <div class="col-lg-4 col-md-4 col-sm-4"
		  <div class="tg-noticeboardarea" style="background-color: #dcdee0; height:444px;  box-shadow: 0 5px 25px 0 rgba(0,56,160,.15);border-width: thick;">

							<div class="tg-widget tg-widgetadmissionform">
								<div class="tg-widgetcontent">
									<h3 class="text-theme-color-sky">Notice Board</h3>
									<hr> </hr>
									<marquee behavior="scroll" direction="up" scrolldelay="150" color="blue" onmouseover="this.stop();" onmouseout="this.start();" style="  height:300px; 
  overflow: hidden;
  overflow-x:-webkit-marquee;
  
  overflow-x: marquee-line;
 ">

									<div class="tg-description" style="color:blue;">
									<?php foreach (array_reverse($files) as $file): ?>
									<p><?php echo $file['headline']; ?></p>
							
									<a href="insider/uploads/<?php echo $file['name'] ?>"><span style="color:red;"><i class="fa fa-file-pdf-o"></i>Download</span></a><hr></hr>
						
									<?php endforeach;?>
							
									
									
								
									</div>
									</marquee>
								</div>
							</div>
						
							</div>
							</div>
        </div>
        <!-- end .rev_slider_wrapper -->
        <script>
          $(document).ready(function(e) {
            $(".rev_slider_default").revolution({
              sliderType:"standard",
              sliderLayout: "auto",
              dottedOverlay: "none",
              delay: 5000,
              navigation: {
                  keyboardNavigation: "off",
                  keyboard_direction: "horizontal",
                  mouseScrollNavigation: "off",
                  onHoverStop: "off",
                  touch: {
                      touchenabled: "on",
                      swipe_threshold: 75,
                      swipe_min_touches: 1,
                      swipe_direction: "horizontal",
                      drag_block_vertical: false
                  },
                arrows: {
                  style: "gyges",
                  enable: true,
                  hide_onmobile: false,
                  hide_onleave: true,
                  hide_delay: 200,
                  hide_delay_mobile: 1200,
                  tmp: '',
                  left: {
                      h_align: "left",
                      v_align: "center",
                      h_offset: 0,
                      v_offset: 0
                  },
                  right: {
                      h_align: "right",
                      v_align: "center",
                      h_offset: 0,
                      v_offset: 0
                  }
                },
                bullets: {
                  enable: true,
                  hide_onmobile: true,
                  hide_under: 800,
                  style: "hebe",
                  hide_onleave: false,
                  direction: "horizontal",
                  h_align: "center",
                  v_align: "bottom",
                  h_offset: 0,
                  v_offset: 30,
                  space: 5,
                  tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title"></span>'
                }
              },
              responsiveLevels: [1240, 1024, 778],
              visibilityLevels: [1240, 1024, 778],
              gridwidth: [1170, 1024, 778, 480],
              gridheight: [640, 768, 960, 720],
              lazyType: "none",
              parallax: {
                  origo: "slidercenter",
                  speed: 1000,
                  levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
                  type: "scroll"
              },
              shadow: 0,
              spinner: "off",
              stopLoop: "on",
              stopAfterLoops: 0,
              stopAtSlide: -1,
              shuffle: "off",
              autoHeight: "off",
              fullScreenAutoWidth: "off",
              fullScreenAlignForce: "off",
              fullScreenOffsetContainer: "",
              fullScreenOffset: "0",
              hideThumbsOnMobile: "off",
              hideSliderAtLimit: 0,
              hideCaptionAtLimit: 0,
              hideAllCaptionAtLilmit: 0,
              debugMode: false,
              fallbacks: {
                  simplifyAll: "off",
                  nextSlideOnWindowFocus: "off",
                  disableFocusListener: false,
              }
            });
          });
        </script>
		
		<style>
		.tg-noticeboardarea{
	width: 25%;
	float: right;
	padding: 0 0 0 20px;

	
	
}
.tg-noticeboardarea .tg-widget:first-child .tg-widgetcontent{background: #52b554;}
.tg-noticeboardarea .tg-widget + .tg-widget{margin: 21px 0 0;}
.tg-noticeboardarea .tg-widget .tg-widgetcontent{padding: 20px;}
		</style>
        <!-- Slider Revolution Ends -->
					  </div>
					  </div>
					  </div>
					  </div>
    </section>

    

    <!-- Section: About -->
    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-6">
              <h2 class="text-theme-color-sky line-bottom">Welcome To <span class="text-theme-color-red">Sky High</span> <br> Global School</h2>
             <p>Sky High Global school is full of fun and activity based an elementary school,
providing valued sense and learning to children. When a child is entering to a new
world of pre-school it is an exciting time . Here we are trying our best to make
education easier and more fun for kids. SHGS aims to offer quality education to
children to help them to grow in a harmonious environment resulting in their all
round development.</p>
               
            </div>
            <div class="col-md-6">
              <div class="video-popup">                
                <a>
                  <img alt="" src="image/6.png" class="img-responsive img-fullwidth">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div> 
          <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
      </div>
    </section>

    <!-- Section: Features -->
    <section class="" data-bg-img="images/bg/p2.jpg">
      <div class="container pb-0">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="line-bottom-center mt-0">What  <span class="text-theme-color-red">Makes us special </span></h2>
              <div class="title-flaticon">
                <i class="flaticon-charity-alms"></i>
              </div>
              
            </div>
          </div>
        </div>
        <div class="section-content">     
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">              
              <div class="row features-style1">
                <div class="col-sm-12">
                  <div class="icon-box left media p-0 mb-sm-10 mt-30 mt-sm-0"> <a href="#" class="icon icon-circled icon-lg border-1px bg-theme-color-red pull-right flip mr-0 ml-30"><i class="fa fa-pencil-square-o text-white"></i></a>
                    <div class="media-body text-right">
                      <h4 class="text-theme-color-blue">Active Learning</h4>
                      <p>Collaborative learning for all round development </p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="icon-box left media p-0 mb-sm-10"> <a href="#" class="icon icon-circled icon-lg border-1px bg-theme-color-yellow  pull-right flip mr-0 ml-30"><i class="fa fa-book text-white"></i></a>
                    <div class="media-body text-right">
                      <h4 class="text-theme-color-sky">Multimedia Class</h4>
                      <p> Technology driven active learning to boost mind and brain co-ordination .</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="icon-box left media p-0 mb-sm-10"> <a href="#" class="icon icon-circled icon-lg border-1px bg-theme-color-lemon  pull-right flip mr-0 ml-30"><i class="fa fa-graduation-cap text-white"></i></a>
                    <div class="media-body text-right">
                      <h4 class="text-theme-color-red">Classroom Safety</h4>
                      <p> Classroom safety with physical and emotional safety standards
 </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
              <img src="image/elephant.gif" height="450px" width="500px">
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">              
              <div class="row features-style1">
                <div class="col-sm-12">
                  <div class="icon-box left media p-0 mb-sm-10 mt-30 mt-sm-30"> <a href="#" class="icon icon-circled icon-lg border-1px bg-theme-color-red  pull-left flip"><i class="fa fa-graduation-cap text-white"></i></a>
                    <div class="media-body">
                      <h4 class="text-theme-color-blue">Expert Teachers</h4>
                      <p>  Teachers have extraordinary levels of patience, enthusiasm and  creativity</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="icon-box left media p-0 mb-sm-10"> <a href="#" class="icon icon-circled icon-lg border-1px bg-theme-color-yellow pull-left flip"><i class="fa fa-book text-white"></i></a>
                    <div class="media-body">
                      <h4 class="text-theme-color-sky">Funny and Happy</h4>
                      <p>  Teaching the child the art of happiness,  kinds of habits to create happiness.</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="icon-box left media p-0 mb-sm-10"> <a href="#" class="icon icon-circled icon-lg border-1px bg-theme-color-lemon pull-left flip"><i class="fa fa-pencil-square-o text-white"></i></a>
                    <div class="media-body">
                      <h4 class="text-theme-color-red">Fulfill With Love</h4>
                      <p>Fosters an atmosphere that is warm, loving and fun .  providing exceptional child care .</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>     
        </div>
      </div>
    </section>



<section  data-bg-img="images/bg/p2.jpg">
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="line-bottom-center mt-0">Our <span class="text-theme-color-red" >Programs</span></h2>
              <div class="title-flaticon">
                <i class="flaticon-charity-alms"></i>
              </div>
              <p style="font-size:16px; text-align: justify;
  " >We offer Turbo Toddler programme (Play Group) Jumbo Nursery(Nursery Group) Dumbo Junior (Lkg)  and Tembo senior (Ukg) . After school we have Music, mordern dance and many more activities we do at at our center. Our Early Years Program focuses on four fundamental development areas – Cognition, Language and Communication; Personal, Social and Emotional development; Creativity and Imagination and Physical Development.</p>

<p style="font-size:16px; text-align: justify;"> These fundamental development areas inform eight specific development areas – Reading, Writing, Understanding subjects, Confidence building, Self Expression, Science and Technology,  Mathematics and Reasoning.

We focus on the childs physical, social and emotional development. Our play way method allows children to learn with fun.  
</p>
            </div>
          </div>
        </div>
        <div>
          <div>
            <div>
              <div style="padding:60px;">
                <ul class="nav nav-tabs">
                  <li ><a href="turbo-toddler-program.html" ><button type="button" class="btn btn-primary">Turbo Toddler <br>(Play Group)</button></a></li>
                  <li ><a href="nursery-school-program.html"> <button type="button" class="btn btn-success">Jumbo Nursery <br>(Nursery)</button></a></li>
                  <li ><a href="lower-kg-school-program.html" ><button type="button" class="btn btn-info">Dumbo junior <br>(LKG) </button></a></li>
                  <li ><a href="upper-kg-program.html" > <button type="button" class="btn btn-warning">
Tembo senior <br>(UKG)</button></a></li>
                 
                </ul>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </section>



   

    <!-- Section: Courses -->
    <section data-bg-img="images/bg/p2.jpg">
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="line-bottom-center mt-0">Extra Curricular   <span class="text-theme-color-red">Activities</span></h2>
              <div class="title-flaticon">
                <i class="flaticon-charity-alms"></i>
              </div>
              <p style="font-size:16px;">  After School Engagement
 
 
 </p>
            </div>
          </div>
        </div>
        <div class="row multi-row-clearfix">
          <div class="col-md-12">
            <div class="owl-carousel-3col" data-nav="true">
              <div class="item">
                <div class="campaign bg-white maxwidth500 mb-30">
                  <div class="thumb">
                    <img src="images/project/12.jpg" alt="" class="img-fullwidth">
                    <div class="campaign-overlay"></div>
                  </div>
                  <div class="course-details clearfix p-20 pt-15">
                    
                    <h3 class="mt-0"><a class="text-theme-color-red" href="#">Dance Classes</a></h3>
                   
                    <p>We all love those dance shows where kids perform amazingly.  this is yet another extra curricular activity which  most children love.</p>
                    
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="campaign bg-white maxwidth500 mb-30">
                  <div class="thumb">
                    <img src="images/project/13.jpg" alt="" class="img-fullwidth">
                    <div class="campaign-overlay"></div>
                  </div>
                  <div class="course-details clearfix p-20 pt-15">
                 
                    <h3 class="mt-0"><a class="text-theme-color-lemon" href="#">Music Classes</a></h3>
                  
                    <p>Children find this activity relaxing and helps build their confidence.</p>
                  
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="campaign bg-white maxwidth500 mb-30">
                  <div class="thumb">
                    <img src="images/project/14.jpg" alt="" class="img-fullwidth">
                    <div class="campaign-overlay"></div>
                  </div>
                  <div class="course-details clearfix p-20 pt-15">
                   
                    <h3 class="mt-0"><a class="text-theme-color-sky" href="#"> Entrepreneurship: A mini-tycoon- 
  </a></h3>
                   
                    <p>Group activity with a few other friends , Do not spoon-feed. provide support .
  </p>
                 
                  </div>
                </div>
              </div>
			  
			  
			      <div class="item">
                <div class="campaign bg-white maxwidth500 mb-30">
                  <div class="thumb">
                    <img src="images/project/25.jpg" alt="" class="img-fullwidth">
                    <div class="campaign-overlay"></div>
                  </div>
                  <div class="course-details clearfix p-20 pt-15">
                   
                    <h3 class="mt-0"><a class="text-theme-color-lemon" href="#"> Keep exploring- 
  </a></h3>
                  
                    <p> Search and discover , Do research and analysis .
  </p>
                  
                  </div>
                </div>
              </div>
			  
              <div class="item">
                <div class="campaign bg-white maxwidth500 mb-30">
                  <div class="thumb">
                    <img src="images/project/15.jpg" alt="" class="img-fullwidth">
                    <div class="campaign-overlay"></div>
                  </div>
                  <div class="course-details clearfix p-20 pt-15">
                   
                    <h3 class="mt-0"><a class="text-theme-color-green" href="#">Painting and Sketching</a></h3>
                   
                    <p>Encourage drawing activities ,</p>
                  
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="campaign bg-white maxwidth500 mb-30">
                  <div class="thumb">
                    <img src="images/project/16.jpg" alt="" class="img-fullwidth">
                    <div class="campaign-overlay"></div>
                  </div>
                  <div class="course-details clearfix p-20 pt-15">
            
                    <h3 class="mt-0"><a class="text-theme-color-orange" href="#">Art and Craft </a></h3>
                 
                    <p>Developing creativity and imagination with  arts and crafts</p>
                  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: Our Gallery -->
    <section>
      <div class="container pb-70">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="line-bottom-center mt-0">Our <span class="text-theme-color-red">Gallery</span></h2>
              <div class="title-flaticon">
                <i class="flaticon-charity-alms"></i>
              </div>
            
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row mb-30">
            <div class="col-xs-12 col-sm-4 col-md-4">
              <div class="gallery-block">
                <div class="gallery-thumb">
                  <img alt="project" src="image/gallery/42.jpg" class="img-fullwidth">
                </div>
                <div class="overlay-shade red"></div>
                <div class="icons-holder">
                  <div class="icons-holder-inner">
                    <div class="gallery-icon">
                      <a href="image/gallery/42.jpg"  data-lightbox-gallery="gallery"><i class="pe-7s-science"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>            
            <div class="col-xs-12 col-sm-4 col-md-4">
              <div class="gallery-block">
                <div class="gallery-thumb">
                  <img alt="project" src="image/gallery/25.jpg" class="img-fullwidth">
                </div>
                <div class="overlay-shade yellow"></div>
                <div class="icons-holder">
                  <div class="icons-holder-inner">
                    <div class="gallery-icon">
                      <a href="image/gallery/25.jpg"  data-lightbox-gallery="gallery"><i class="pe-7s-science"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4">
              <div class="gallery-block">
                <div class="gallery-thumb">
                  <img alt="project" src="image/gallery/3.jpg" class="img-fullwidth">
                </div>
                <div class="overlay-shade green"></div>
                <div class="icons-holder">
                  <div class="icons-holder-inner">
                    <div class="gallery-icon">
                      <a href="image/gallery/3.jpg"  data-lightbox-gallery="gallery"><i class="pe-7s-science"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>  
          </div>
          <div class="row mb-30">
            <div class="col-xs-12 col-sm-4 col-md-4">
              <div class="gallery-block">
                <div class="gallery-thumb">
                  <img alt="project" src="image/gallery/4.jpg" class="img-fullwidth">
                </div>
                <div class="overlay-shade green"></div>
                <div class="icons-holder">
                  <div class="icons-holder-inner">
                    <div class="gallery-icon">
                      <a href="image/gallery/4.jpg"  data-lightbox-gallery="gallery"><i class="pe-7s-science"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>            
            <div class="col-xs-12 col-sm-4 col-md-4">
              <div class="gallery-block">
                <div class="gallery-thumb">
                  <img alt="project" src="image/gallery/7.jpeg" class="img-fullwidth">
                </div>
                <div class="overlay-shade blue"></div>
                <div class="icons-holder">
                  <div class="icons-holder-inner">
                    <div class="gallery-icon">
                      <a href="image/gallery/7.jpeg"  data-lightbox-gallery="gallery"><i class="pe-7s-science"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>            
            <div class="col-xs-12 col-sm-4 col-md-4">
              <div class="gallery-block">
                <div class="gallery-thumb">
                  <img alt="project" src="image/gallery/20.jpeg" class="img-fullwidth">
                </div>
                <div class="overlay-shade yellow"></div>
                <div class="icons-holder">
                  <div class="icons-holder-inner">
                    <div class="gallery-icon">
                      <a href="image/gallery/20.jpeg"  data-lightbox-gallery="gallery"><i class="pe-7s-science"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

   <!-- Section: VIDEO -->
    <section id="pricing" class="bg-lighter">
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="line-bottom-center mt-0">Our <span class="text-theme-color-red">Videos</span></h2>
              <div class="title-flaticon">
                <i class="flaticon-charity-alms"></i>
              </div>
               </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 hvr-float-shadow mb-sm-30">
              <div class="pricing-table maxwidth400">
               
              <iframe width="420" height="315"
src="https://www.youtube.com/embed/_fuHdf6r2CU">
</iframe>
                
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 hvr-float-shadow mb-sm-30">
              <div class="pricing-table maxwidth400">
                <iframe width="420" height="315"
src="https://www.youtube.com/embed/rDEcMpo5Htc">
</iframe>
                
                
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 hvr-float-shadow mb-sm-30">
              <div class="pricing-table maxwidth400">
			  <iframe width="420" height="315"
src="https://www.youtube.com/embed/K6f7gzzSHVw">
</iframe>
               
                </div>
            </div>
          </div>
        </div>
      </div>
    </section>


    <!-- Section: Why Choose Us -->
    <section>
      <div class="container">
         <div class="section-content">
         <div class="row">
            <div class="col-md-5"> 
             <img src="image/3.png" class="img-fullwidth" alt="">
            </div>
            <div class="col-md-7 pb-sm-20">
              <h2 class="title ">Why <span class="text-theme-color-red">Choose Us</span> ?</h2>
              <p class="mb-20">Our educational staffs are qualified and well trained. We ensure that your little ones will be given proper attention and guidance and it will help them to explore new things. SHGSchool would help you in your journey of bringing up your little one by providing the right education, values and ideas, as your little one gets ready to step into the world!</p>
            
          
              
            </div>
         </div>
         </div>
       </div>
      <div> 
          <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
      </div>
    </section>

    <!-- Divider: testimonials -->
    <section class="divider" data-bg-img="images/bg/b1.jpg">
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="line-bottom-center mt-0">Happy<span class="text-theme-color-red"> Parent's Say</span></h2>
              <p></p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="owl-carousel-2col testimonial style2 dots-white" data-dots="false">
              <div class="item">
                <div class="testimonial-wrapper">                  
                  <div class="content bg-theme-color-orange p-30 pb-40">
                    <p class="font-15 text-white font-weight-600">Sending my daughter to SHG school was the best decision till now. In a sea of preschools in my area SHG shone like a diamond for us. My kid was very shy and anxious which was the reason why I wanted a home like teaching environment for her and SHG gave me that along with the playful learning atmosphere, smart school activities, outdoor educational tours and qualified teachers. The transition from home to school was a smooth process, we have Buddy, our child's Fun Partner at SHG school. I can proudly say my daughter has transformed from caterpillar to butterfly with the help of SHG school and I am really thankful to the teachers of SHG school and my best wishes in their future endeavours. 

</p>
                    <i class="fa fa-quote-right mt-10 text-white"></i>
                    <h4 class="author text-white mt-20 mb-0">Dr Padmini Rani</h4>
                    <h6 class="title text-white mt-0 mb-15"></h6>
                    <div class="thumb mt-20"><img class="img-circle" alt="" src="images/testimonials/0.jpg"></div>
                  </div>                  
                </div>
              </div>
              <div class="item">
                <div class="testimonial-wrapper">                  
                  <div class="content bg-theme-color-lemon p-30 pb-40">
                    <p class="font-15 text-white font-weight-600">When a small baby come out from their Mama's lap to any other world, baby need exactly same environment to grow. I must say SHG School exactly the same place a baby can grow. We are very much impressed with the teaching methodology, learn through play. The School environment is very much neat and clean , your toddler will get the enough space to play and enjoy .We are very much Happy to say, our baby start her career from SHG School. Thank you SHG School for the wonderful opportunity.</p>
                    <i class="fa fa-quote-right mt-10 text-white"></i>
                    <h4 class="author text-white mt-20 mb-0">Mr. Alok Ranjan Nayak</h4>
                    <h6 class="title text-white mt-0 mb-15"></h6>
                    <div class="thumb mt-20"><img class="img-circle" alt="" src="images/testimonials/Mr-Alok -Ranjan-Nayak.jpg"></div>
                  </div>                  
                </div>
              </div>
              <div class="item">
                <div class="testimonial-wrapper">                  
                  <div class="content bg-theme-color-red p-30 pb-40">
                    <p class="font-15 text-white font-weight-600">  The Sky high global school is one of the best school for the students. So far the disciplinary and performance of the students is concerned is highly satisfactory. The method of teaching  is unique. Besides teaching from books,  varieties of Play instruments are there for the kids. Other activities like dance and Song are taught in the school. Regular report cards are sent to the parents.

The amicable personality of the Headmistress is highly praise worldly.

 Above all we the parents like the school very much.

</p>
                    <i class="fa fa-quote-right mt-10 text-white"></i>
                    <h4 class="author text-white mt-20 mb-0"> Mr. Lingaraj Prusty</h4>
                    <h6 class="title text-white mt-0 mb-15"></h6>
                    <div class="thumb mt-20"><img class="img-circle" alt="" src="images/testimonials/Mr-Lingaraj-Prusty.jpg"></div>
                  </div>                  
                </div>
              </div>
              
              
                 <div class="item">
                <div class="testimonial-wrapper">                  
                  <div class="content bg-theme-color-lemon p-30 pb-40">
                    <p class="font-15 text-white font-weight-600">   All the teachers and staffs are very caring and friendly. They are providing  extracurricular activities after school hours like music, dance, phonics and many more. I am happy that my son is studying in a safe and secure environment. 

</p>
                    <i class="fa fa-quote-right mt-10 text-white"></i>
                    <h4 class="author text-white mt-20 mb-0">  Ansura Biwi</h4>
                    <h6 class="title text-white mt-0 mb-15"></h6>
                    <div class="thumb mt-20"><img class="img-circle" alt="" src="images/testimonials/4.jpg"></div>
                  </div>                  
                </div>
              </div>
              
              
              
              
              
              
                 <div class="item">
                <div class="testimonial-wrapper">                  
                  <div class="content bg-theme-color-red p-30 pb-40">
                    <p class="font-15 text-white font-weight-600">   Sky high global school is a modern school with all type of latest facilities available here. It has stress free environment which a child required. Teachers are very competent and caring. Education system is really good. Their play way method of learning is really best. We really love the curricular activities. They celebrates each and every occasion in a different way. After joining the school I found my daughter has developed in her studies and also in other activities. We are glad that we choose this school for our daughter Dristi. 

</p>
                    <i class="fa fa-quote-right mt-10 text-white"></i>
                    <h4 class="author text-white mt-20 mb-0">  Mrs Itishree Sethy</h4>
                    <h6 class="title text-white mt-0 mb-15"></h6>
                    <div class="thumb mt-20"><img class="img-circle" alt="" src="images/testimonials/5.jpg"></div>
                  </div>                  
                </div>
              </div>
              
              
            </div>
          </div>
        </div>
      </div>
    </section>


   


  <!-- Footer -->
  <footer id="footer" class="footer divider layer-overlay overlay-dark-9" data-bg-img="images/bg/bg8.jpg">
    <div class="container">
      <div class="row border-bottom">
        <div class="col-sm-9 col-md-4">
          <div class="widget dark">
            <img class="mt-5 mb-20" alt="" src="image/logo2.png">
            <p>Plot No. LP-899,

Ground Floor,

Near Hanuman Temple, Prashanti Vihar,

Patia, Bhubaneshwar,

Odisha 751024</p>
            <ul class="list-inline mt-5">
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-theme-color-red mr-5"></i> <a class="text-gray" href="#">  +91 7008776394</a> </li>
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-theme-color-lemon mr-5"></i> <a class="text-gray" href="mailto: mail@shgschools.com">mail@shgschools.com</a> </li>
              
			  </ul>
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="widget dark">
            <h4 class="widget-title">Useful Links</h4>
            <ul class="list angle-double-right list-border">
              <li><a href="index.php">Home</a></li>
			  <li><a href="about.html">About Us</a></li>
			  <li><a href="gallery.html">Gallery</a></li>
              <li><a href="contact-us.html">Contact Us</a></li>
              <li><a href="faqs.html">Faqs</a></li>
             
			  <li><a href="shgs-support.html">SHGS Support</a></li>
			  <li><a href="application-form.html">Application Form</a></li>
                            
            </ul>
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="widget dark">
            <h4 class="widget-title">Facebook feed</h4>
            
			<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fskyhighglobalschool&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1807552176029053" width="400" height="500" style="border:none;overflow:hidden" scrolling="no" 
			frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

			
			</div>
          </div>
        </div>
     
      </div>
      <div class="row mt-30">
        <div class="col-md-9">
          <div class="widget dark" style="margin-left:100px;">
            <h5 class="widget-title mb-10">Call Us Now</h5>
            <div class="text-white">
             +91 7008776394
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="widget dark" style="margin-left:100px;">
            <h5 class="widget-title mb-10">Connect With Us</h5>
            <ul class="styled-icons icon-bordered icon-sm">
              <li><a href="https://www.facebook.com/skyhighglobalschool/"><i class="fa fa-facebook"></i></a></li>
        
            
            
              <li><a href="https://www.instagram.com/skyhighglobalbbsr/"><i class="fa fa-instagram"></i></a></li>
           
            </ul>
          </div>
        </div>
        
      </div>
    </div>
    <div class="footer-bottom bg-black-333">
      <div class="container pt-20 pb-20">
        <div class="row">
          <div class="col-md-6">
            <p class="font-12" style="color:white;">&copy; Sky High Global School,2019 | All Rights Reserved &nbsp; &nbsp; Developed By <a href="https://wiselab.in" style="color:#00BCD4;">Wiselab technologies.</a> </p>
			
		  </div>
		  
		  
		  <div  class="col-md-6" >
		
		  <p style="color:white;">Total Visitor: <img src='https://www.counter12.com/img-zzwAW2BaA993AxaW-26.gif' border='0' alt='counter'></p> 
		  
		  <script type='text/javascript' src='https://www.counter12.com/ad.js?id=zzwAW2BaA993AxaW'>
		  </script>
		  </div>
		
			
        </div>
      </div>
    </div>
  </footer>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="js/custom.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.video.min.js"></script>

</body>

<!-- Mirrored from html.kodesolution.live/s/kidspro/v2.1/demo/index-mp-layout1.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 06 Jul 2019 13:04:40 GMT -->
</html>
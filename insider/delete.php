<?php


require('session.php');


if(isset($_GET['id']))
{
     $sql = "DELETE FROM notice WHERE id=".$_GET['id'];
     $con->query($sql);
	// echo 'Deleted successfully.';
	 
	
}
 header("Refresh: 3;url=view_notice.php");


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>Wiselab Business Insider ||  Empowering Business</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="./assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="./assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="./assets/vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- PLUGINS STYLES-->
    <!-- THEME STYLES-->
    <link href="assets/css/main.min.css" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <header class="header">
            <div class="page-brand">
                <a class="link" href="dashboard.php">
                    <span class="brand">Wiselab</span>
                        <span class="brand-tip">Insider</span>
                </a>
            </div>
            <div class="flexbox flex-1">
                <!-- START TOP-LEFT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                    <li>
                        <a class="nav-link sidebar-toggler js-sidebar-toggler"><i class="ti-menu"></i></a>
                    </li>
                    <li>
                        <form class="navbar-search" action="javascript:;">
                            <div class="rel">
                                <span class="search-icon"><i class="ti-search"></i></span>
                                <input class="form-control" placeholder="Search here...">
                            </div>
                        </form>
                    </li>
                </ul>
                <!-- END TOP-LEFT TOOLBAR-->
                <!-- START TOP-RIGHT TOOLBAR-->
                <ul class="nav navbar-toolbar">
         
                    <li class="dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                            <img src="./assets/img/admin-avatar.png" />
                            <span></span>Admin<i class="fa fa-angle-down m-l-5"></i></a>
                        <ul class="dropdown-menu dropdown-menu-right">
                           
                            <li class="dropdown-divider"></li>
                            <a class="dropdown-item" href="logout.php"><i class="fa fa-power-off"></i>Logout</a>
                        </ul>
                    </li>
                </ul>
                <!-- END TOP-RIGHT TOOLBAR-->
            </div>
        </header>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        <nav class="page-sidebar" id="sidebar">
            <div id="sidebar-collapse">
                <div class="admin-block d-flex">
                    <div>
                        <img src="./assets/img/admin-avatar.png" width="45px" />
                    </div>
                    <div class="admin-info">
                        <div class="font-strong"><?php echo $_SESSION['login_user'];?></div><small>Administrator</small></div>
                </div>
                  <ul class="side-menu metismenu">
                    <li>
                        <a class="active" href="dashboard.php"><i class="sidebar-item-icon fa fa-th-large"></i>
                            <span class="nav-label">Dashboard</span>
                        </a>
                    </li>
                    <li class="heading">FEATURES</li>
                    <li>
                        <a href="datatables.php"><i class="sidebar-item-icon fa fa-envelope"></i>
                            <span class="nav-label">Contact Us Queries</span><i class="fa fa-angle-left arrow"></i></a>
                      
                    </li>
					
					
						 <li>
                        <a href="datatables2.php"><i class="sidebar-item-icon fa fa-envelope"></i>
                            <span class="nav-label">Franchise Query</span><i class="fa fa-angle-left arrow"></i></a>
                      
                    </li>
						 <li>
                        <a href="admission2.php"><i class="sidebar-item-icon fa fa-envelope"></i>
                            <span class="nav-label">Admission Inquery</span><i class="fa fa-angle-left arrow"></i></a>
                      
                    </li>
                    <li>
                        <a href="javascript:;"><i class="sidebar-item-icon fa fa-edit"></i>
                            <span class="nav-label">Notice Board</span><i class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-2-level collapse">
                            
                            <li>
                                <a href="add_notice.php">Add Notice</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="view_notice.php"><i class="sidebar-item-icon fa fa-table"></i>
                            <span class="nav-label">View Notice</span><i class="fa fa-angle-left arrow"></i></a>
                       
                    </li>
                
                 
                 
                  
                </ul>
            </div>
        </nav>
        <!-- END SIDEBAR-->
		<div class="content-wrapper">
            <!-- START PAGE CONTENT-->
          
          
           
               	<header class="site-header" id="header">
		<h1 class="site-header__title" data-lead-id="site-header-title">THANK YOU! NOTICE DELETED SUCCESSFULLY</h1>
	</header>

	<div class="main-content">
	
<img src="../image/sent.gif">
		<p class="main-content__body" data-lead-id="main-content-body">Thanks For Your Great Time.  </p>

    
              
            
               
          
            <!-- END PAGE CONTENT-->
            <footer class="page-footer">
             <div class="font-13">2019 © <b>WiselabInsider</b> - All rights reserved.</div>
             
                <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
            </footer>
        </div>
    </div>
   
	
		
	</script>
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
    <!-- CORE PLUGINS-->
    <script src="./assets/vendors/jquery/dist/jquery.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/popper.js/dist/umd/popper.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/metisMenu/dist/metisMenu.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL PLUGINS-->
    <!-- CORE SCRIPTS-->
    <script src="assets/js/app.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL SCRIPTS-->
</body>

</html>

<?php
 include 'session.php';

?>


<?php

$res = mysqli_query($con,"SELECT * FROM contact_us") or die('Error');
$val=mysqli_fetch_array($res);
$tot=mysqli_num_rows($res);

?>
<?php
$result = mysqli_query($con,"SELECT * FROM franchise") or die('Error');
$values=mysqli_fetch_array($result);
$total=mysqli_num_rows($result);

?>




<?php

$res3 = mysqli_query($con,"SELECT * FROM notice") or die('Error');
$val4=mysqli_fetch_array($res3);
$tot5=mysqli_num_rows($res3);

?>

<?php

$res4 = mysqli_query($con,"SELECT * FROM admission") or die('Error');
$val5=mysqli_fetch_array($res4);
$tot6=mysqli_num_rows($res4);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>Wiselab Business Insider</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="./assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="./assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="./assets/vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />
    <!-- PLUGINS STYLES-->
    <link href="./assets/vendors/jvectormap/jquery-jvectormap-2.0.3.css" rel="stylesheet" />
    <!-- THEME STYLES-->
    <link href="assets/css/main.min.css" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <header class="header">
            <div class="page-brand">
                <a class="link" href="dashboard.php">
                   <span class="brand">Wiselab</span>
                        <span class="brand-tip">Insider</span>
                </a>
            </div>
            <div class="flexbox flex-1">
                <!-- START TOP-LEFT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                    <li>
                        <a class="nav-link sidebar-toggler js-sidebar-toggler"><i class="ti-menu"></i></a>
                    </li>
                    
                </ul>
                <!-- END TOP-LEFT TOOLBAR-->
                <!-- START TOP-RIGHT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                 
                    
                    <li class="dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                            <img src="./assets/img/admin-avatar.png" />
                            <span></span>Admin<i class="fa fa-angle-down m-l-5"></i></a>
                        <ul class="dropdown-menu dropdown-menu-right">
                         
                            <li class="dropdown-divider"></li>
                            <a class="dropdown-item" href="logout.php"><i class="fa fa-power-off"></i>Logout</a>
                        </ul>
                    </li>
                </ul>
                <!-- END TOP-RIGHT TOOLBAR-->
            </div>
        </header>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        <nav class="page-sidebar" id="sidebar">
            <div id="sidebar-collapse">
                <div class="admin-block d-flex">
                    <div>
                        <img src="./assets/img/admin-avatar.png" width="45px" />
                    </div>
                    <div class="admin-info">
                        <div class="font-strong"><?php echo $_SESSION['login_user'];?></div><small>Administrator</small></div>
                </div>
                <ul class="side-menu metismenu">
                    <li>
                        <a class="active" href="dashboard.php"><i class="sidebar-item-icon fa fa-th-large"></i>
                            <span class="nav-label">Dashboard</span>
                        </a>
                    </li>
                    <li class="heading">FEATURES</li>
                    <li>
                        <a href="datatables.php"><i class="sidebar-item-icon fa fa-envelope"></i>
                            <span class="nav-label">Contact Us Queries</span><i class="fa fa-angle-left arrow"></i></a>
                      
                    </li>
					
					 <li>
                        <a href="datatables2.php"><i class="sidebar-item-icon fa fa-envelope"></i>
                            <span class="nav-label">Franchise Query</span><i class="fa fa-angle-left arrow"></i></a>
                      
                    </li>
					
					 <li>
                        <a href="admission2.php"><i class="sidebar-item-icon fa fa-envelope"></i>
                            <span class="nav-label">Admission Inquery</span><i class="fa fa-angle-left arrow"></i></a>
                      
                    </li>
                    <li>
                        <a href="javascript:;"><i class="sidebar-item-icon fa fa-edit"></i>
                            <span class="nav-label">Notice Board</span><i class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-2-level collapse">
                            
                            <li>
                              
								  <a href="add_notice.php">Add Notice</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="view_notice.php"><i class="sidebar-item-icon fa fa-table"></i>
                            <span class="nav-label">View Notice</span><i class="fa fa-angle-left arrow"></i></a>
                      
                    </li>
                  
                
                 
                 
                  
                </ul>
            </div>
        </nav>
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-success color-white widget-stat">
                           <div class="ibox-body">
                              <h2 class="m-b-5 font-strong"><?php echo $tot;?></h2>
                                <div class="m-b-5">Contact Us Queries</a></div><i class="ti-user widget-stat-icon"></i>
                             
                            </div>
						</div>
					</div>
					  <div class="col-lg-3 col-md-6">
					 <div class="ibox bg-yellow color-white widget-stat">
                          <div class="ibox-body">
                                <h2 class="m-b-5 font-strong"><?php echo $total;?></h2>
                                <div class="m-b-5">Franchise Query</div><i class="ti-user widget-stat-icon"></i>
                            
                          </div>
						</div>
						</div>
						
						 <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-info color-white widget-stat">
                            <div class="ibox-body">
                                <h2 class="m-b-5 font-strong"><?php echo $tot6;?></h2>
                                <div class="m-b-5"> Admission Inquery</div><i class="ti-user widget-stat-icon"></i>
                                
                            </div>
                        </div>
                    </div>
				
                    <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-teal color-white widget-stat">
                            <div class="ibox-body">
                                <h2 class="m-b-5 font-strong"><?php echo $tot5;?></h2>
                                <div class="m-b-5">Total No of Notice</div><i class="ti-bar-chart widget-stat-icon"></i>
                                
                            </div>
                        </div>
                    </div>
                   	
                   
                  
                </div>
                
          
                <style>
                    .visitors-table tbody tr td:last-child {
                        display: flex;
                        align-items: center;
                    }

                    .visitors-table .progress {
                        flex: 1;
                    }

                    .visitors-table .progress-parcent {
                        text-align: right;
                        margin-left: 10px;
                    }
                </style>
              
            </div>
            <!-- END PAGE CONTENT-->
            <footer class="page-footer">
                <div class="font-13">2019 © <b>WiselabInsider</b> - All rights reserved.</div>
               
                <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
            </footer>
        </div>
    </div>
    
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
    <!-- CORE PLUGINS-->
    <script src="./assets/vendors/jquery/dist/jquery.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/popper.js/dist/umd/popper.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/metisMenu/dist/metisMenu.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL PLUGINS-->
    <script src="./assets/vendors/chart.js/dist/Chart.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/jvectormap/jquery-jvectormap-2.0.3.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <script src="./assets/vendors/jvectormap/jquery-jvectormap-us-aea-en.js" type="text/javascript"></script>
    <!-- CORE SCRIPTS-->
    <script src="assets/js/app.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL SCRIPTS-->
    <script src="./assets/js/scripts/dashboard_1_demo.js" type="text/javascript"></script>
</body>

</html>


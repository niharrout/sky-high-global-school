<?php
// connect to the database
$conn = mysqli_connect('localhost','shgschoo_shg','wiselab@995','shgschoo_shg');

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>Wiselab Business Insider ||  Empowering Business</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="./assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="./assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="./assets/vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- PLUGINS STYLES-->
    <!-- THEME STYLES-->
    <link href="assets/css/main.min.css" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <header class="header">
            <div class="page-brand">
                <a class="link" href="dashboard.php">
                    <span class="brand">Wiselab</span>
                        <span class="brand-tip">Insider</span>
                </a>
            </div>
            <div class="flexbox flex-1">
                <!-- START TOP-LEFT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                    <li>
                        <a class="nav-link sidebar-toggler js-sidebar-toggler"><i class="ti-menu"></i></a>
                    </li>
                  
                </ul>
                <!-- END TOP-LEFT TOOLBAR-->
                <!-- START TOP-RIGHT TOOLBAR-->
                <ul class="nav navbar-toolbar">
         
                    <li class="dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                            <img src="./assets/img/admin-avatar.png" />
                            <span></span>Admin<i class="fa fa-angle-down m-l-5"></i></a>
                        <ul class="dropdown-menu dropdown-menu-right">
                           
                            <li class="dropdown-divider"></li>
                            <a class="dropdown-item" href="logout.php"><i class="fa fa-power-off"></i>Logout</a>
                        </ul>
                    </li>
                </ul>
                <!-- END TOP-RIGHT TOOLBAR-->
            </div>
        </header>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        <nav class="page-sidebar" id="sidebar">
            <div id="sidebar-collapse">
                <div class="admin-block d-flex">
                    <div>
                        <img src="./assets/img/admin-avatar.png" width="45px" />
                    </div>
                    <div class="admin-info">
                        <div class="font-strong"><?php echo $_SESSION['login_user'];?></div><small>Administrator</small></div>
                </div>
                  <ul class="side-menu metismenu">
                    <li>
                        <a class="active" href="dashboard.php"><i class="sidebar-item-icon fa fa-th-large"></i>
                            <span class="nav-label">Dashboard</span>
                        </a>
                    </li>
                    <li class="heading">FEATURES</li>
                    <li>
                        <a href="datatables.php"><i class="sidebar-item-icon fa fa-envelope"></i>
                            <span class="nav-label">Contact Us Queries</span><i class="fa fa-angle-left arrow"></i></a>
                      
                    </li>
					
					
						 <li>
                        <a href="datatables2.php"><i class="sidebar-item-icon fa fa-envelope"></i>
                            <span class="nav-label">Franchise Query</span><i class="fa fa-angle-left arrow"></i></a>
                      
                    </li>
						 <li>
                        <a href="admission3.php"><i class="sidebar-item-icon fa fa-envelope"></i>
                            <span class="nav-label">Admission Inquery</span><i class="fa fa-angle-left arrow"></i></a>
                      
                    </li>
                    <li>
                        <a href="javascript:;"><i class="sidebar-item-icon fa fa-edit"></i>
                            <span class="nav-label">Notice Board</span><i class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-2-level collapse">
                            
                            <li>
                                <a href="add_notice.php">Add Notice</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="view_notice.php"><i class="sidebar-item-icon fa fa-table"></i>
                            <span class="nav-label">View Notice</span><i class="fa fa-angle-left arrow"></i></a>
                       
                    </li>
                
                 
                 
                  
                </ul>
            </div>
        </nav>
        <!-- END SIDEBAR-->
	























<div class="content-wrapper">
            <!-- START PAGE CONTENT-->
          
    <?php 
    
    // Uploads files
if (isset($_POST['save'])) { // if save button on the form is clicked
    // name of the uploaded file
    $filename = $_FILES['myfile']['name'];
	$notice=$_POST['notice'];

	$random = rand(0,999999999);

    $name_new = ($random.$filename);


    // destination of the file on the server
    $destination = 'uploads/' . $name_new;




    // get the file extension
    $extension = pathinfo($filename, PATHINFO_EXTENSION);

    // the physical file on a temporary uploads directory on the server
    $file = $_FILES['myfile']['tmp_name'];
    $size = $_FILES['myfile']['size'];

    if (!in_array($extension, [ 'pdf', 'docx'])) {
        echo "You file extension must be .zip, .pdf or .docx";
    } elseif ($_FILES['myfile']['size'] > 20000000) { // file shouldn't be larger than 20Megabyte
        echo "File too large!";
    } else {
        // move the uploaded (temporary) file to the specified destination
        if (move_uploaded_file($file, $destination)) {
            $sql = "INSERT INTO notice (name,headline,size) VALUES ('$name_new', '$notice','$size')";
            if (mysqli_query($conn, $sql)) {
       echo  '<b style="color:blue;font-size:30px;font-family:calibri ;">
     File Uploaded Successfully Thank You </b><br> ';
     
     echo '<img src="../image/sent.gif">';
      }
        } else {
         echo '<b style="color:red;font-size:30px;font-family:calibri ;">
     File Upload Faield </b> ';;
        }
    }
}

?>
    
    
    
        
           
    
    
              
            
               
          
            <!-- END PAGE CONTENT-->
            <footer class="page-footer">
             <div class="font-13">2019 © <b>WiselabInsider</b> - All rights reserved.</div>
             
                <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
            </footer>
        </div>
    </div>
   
	
		
	</script>
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
    <!-- CORE PLUGINS-->
    <script src="./assets/vendors/jquery/dist/jquery.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/popper.js/dist/umd/popper.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/metisMenu/dist/metisMenu.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL PLUGINS-->
    <!-- CORE SCRIPTS-->
    <script src="assets/js/app.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL SCRIPTS-->
</body>

</html>





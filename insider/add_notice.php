<?php
 include("session.php");
 
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>Wiselab Business Insider ||  Empowering Business</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="./assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="./assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="./assets/vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />
    <!-- PLUGINS STYLES-->
    <!-- THEME STYLES-->
    <link href="assets/css/main.min.css" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
</head>
<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <header class="header">
            <div class="page-brand">
                <a class="link" href="dashboard.php">
                    <span class="brand">Wiselab
                        <span class="brand-tip">Insider</span>
                    </span>
                 
                </a>
            </div>
            <div class="flexbox flex-1">
                <!-- START TOP-LEFT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                    <li>
                        <a class="nav-link sidebar-toggler js-sidebar-toggler"><i class="ti-menu"></i></a>
                    </li>
                    
                </ul>
                <!-- END TOP-LEFT TOOLBAR-->
                <!-- START TOP-RIGHT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                 
                    <li class="dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                            <img src="./assets/img/admin-avatar.png" />
                            <span></span>Admin<i class="fa fa-angle-down m-l-5"></i></a>
                        <ul class="dropdown-menu dropdown-menu-right">
                         
                            <li class="dropdown-divider"></li>
                            <a class="dropdown-item" href="logout.php"><i class="fa fa-power-off"></i>Logout</a>
                        </ul>
                    </li>
                </ul>
                <!-- END TOP-RIGHT TOOLBAR-->
            </div>
        </header>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        <nav class="page-sidebar" id="sidebar">
            <div id="sidebar-collapse">
                <div class="admin-block d-flex">
                    <div>
                        <img src="./assets/img/admin-avatar.png" width="45px" />
                    </div>
                    <div class="admin-info">
                        <div class="font-strong"><?php echo $_SESSION['login_user'];?></div><small>Administrator</small></div>
                </div>
               <ul class="side-menu metismenu">
                    <li>
                        <a class="active" href="dashboard.php"><i class="sidebar-item-icon fa fa-th-large"></i>
                            <span class="nav-label">Dashboard</span>
                        </a>
                    </li>
                    <li class="heading">FEATURES</li>
                    <li>
                        <a href="datatables.php"><i class="sidebar-item-icon fa fa-envelope"></i>
                            <span class="nav-label">Contact Us Queries</span><i class="fa fa-angle-left arrow"></i></a>
                      
                    </li>
					
					
						 <li>
                        <a href="datatables2.php"><i class="sidebar-item-icon fa fa-envelope"></i>
                            <span class="nav-label">Franchise Query</span><i class="fa fa-angle-left arrow"></i></a>
                      
                    </li>
						 <li>
                        <a href="admission2.php"><i class="sidebar-item-icon fa fa-envelope"></i>
                            <span class="nav-label">Admission Inquery</span><i class="fa fa-angle-left arrow"></i></a>
                      
                    </li>
					
                    <li>
                        <a href="javascript:;"><i class="sidebar-item-icon fa fa-edit"></i>
                            <span class="nav-label">Notice Board</span><i class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-2-level collapse">
                            
                            <li>
                                <a href="add_notice.php">Add Notice</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="view_notice.php"><i class="sidebar-item-icon fa fa-table"></i>
                            <span class="nav-label">View Notice</span><i class="fa fa-angle-left arrow"></i></a>
                        
                    </li>
                
                 
                 
                  
                </ul>
            </div>
        </nav>
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title"><b>ADD NOTICE</b></h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                   
                </ol>
            </div>
            <div class="page-content fade-in-up">
                <div class="row">
                    <div class="col-md-9">
                        
                         
                            <div class="ibox-body">
                               <form action="filesLogic.php" method="post" enctype="multipart/form-data" >
                                    <div class="row">
                                        <div class="col-sm-9 form-group">
                                            <label><b>Enter Notice Headline</b></label>
                                            <input class="form-control" type="text" name="notice" placeholder="Notice Headline" required="">
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="form-group">
                                        <label><b>Upload File</b> <h5 style="color:red;">(Upload only PDF file)</h3></label><br>
                                         <input type="file" name="myfile" placeholder="file upload" required=""> 
                                    </div>
                                   
                                    <div class="form-group">
                                        <button class="btn btn-info" type="submit" name="save">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
     
                </div>
             
                
            </div>
            <!-- END PAGE CONTENT-->
            <footer class="page-footer">
                <div class="font-13">2019 © <b>WiselabInsider</b> - All rights reserved.</div>

                <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
            </footer>
        </div>
    </div>
   
    <!-- CORE PLUGINS-->
    <script src="./assets/vendors/jquery/dist/jquery.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/popper.js/dist/umd/popper.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/metisMenu/dist/metisMenu.min.js" type="text/javascript"></script>
    <script src="./assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL PLUGINS-->
    <!-- CORE SCRIPTS-->
    <script src="assets/js/app.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL SCRIPTS-->
</body>

</html>
-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 04, 2019 at 09:44 PM
-- Server version: 5.7.28
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shgschoo_shg`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `pass`, `date`) VALUES
(1, 'SHG', 'admin@shg.com', 'shgadmin', '2019-08-16 21:02:45'),
(2, 'Mr Admin', 'admin@wiselab.in', 'admin', '2019-08-19 23:44:50');

-- --------------------------------------------------------

--
-- Table structure for table `admission`
--

CREATE TABLE `admission` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `age` int(255) NOT NULL,
  `parents` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `admission_for` varchar(255) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `date_of_submit` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `subject` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `phone`, `email`, `date_of_submit`, `subject`, `message`) VALUES
(3, 'Denisha Ingalls', '08022 92 06 79', 'denisha.ingalls8@hotmail.com', '2019-10-25 12:56:49', 'Increase ranks with related backlinks', 'Having related backlinks is a must in today`s SEO world\r\n\r\nSo, we are able to provide you with this great service at a mere price\r\nhttps://www.monkeydigital.co/product/related-backlinks/\r\n\r\nYou will receive full report within 15 days\r\n500 to 1000 related '),
(1, 'Lucas Weber', '417 59 560', 'info@wrldclass-solutions.com', '2019-09-29 09:23:42', 'Can We  Publish Your Guest Post ?', 'Good Day,\r\n\r\nLucas Weber Here from World Class Solutions, wondering \r\ncan we publish your blog post over here? We are looking to \r\npublish new content and would love to hear about any new products,\r\nor new subjects regarding your website here at shgschool'),
(2, 'DJEdwardHeisk', '82227464155', 'markus2000@op.pl', '2019-10-04 12:45:01', '0DAY MP3 Server FTP Music', 'Hello, \r\n \r\nMusic Private FTP, Exclusive Promo Quality 320kbps, Scene Music. \r\nhttp://0daymusic.org/premium.php \r\n \r\nRegards, \r\n0DAY Music'),
(6, 'Jina Koss', '419 33 960', 'noreplygooglealexarank@gmail.com', '2019-11-25 06:19:00', 'Whitehat Monthly SEO Plans for  shgschools.com', 'Increase ranks and visibility for shgschools.com with a monthly SEO plan that is built uniquely for your website\r\n\r\nIncrease SEO metrics and ranks while receiving complete reports on monthly basis\r\n\r\nCheck out our plans\r\nhttps://googlealexarank.com/index.'),
(4, 'Allie Herz', '021 944 15 82', 'noreplymonkeydigital@gmai.com', '2019-11-14 04:42:25', 'NEW: Rare DA 50+ backlinks', 'Get backlinks from websites which have Domain Authority above 50. Very rare and hard to get backlinks. Order today at a very low price, while the offer lasts.\r\n\r\nread more:\r\nhttps://www.monkeydigital.co/product/250-da-50-backlinks/\r\n\r\nthanks and regards\r\n'),
(5, 'Abdul Wahed', '9440529083', 'wahed215@gmail.com', '2019-11-14 18:02:53', 'Digital School (School Management Software)  Brochure', 'Digital School (School Management Software) \r\n\r\nDear Sir / Madam,\r\n\r\nDigital School is a web and desktop based management information system dedicated for educational institutes to simplify the complexities in administration process and leverage the effic'),
(7, 'Gregoryclext', '83159783996', 'yourmail@gmail.com', '2019-12-04 20:34:40', 'Test, just a test', 'Hello. And Bye.');

-- --------------------------------------------------------

--
-- Table structure for table `franchise`
--

CREATE TABLE `franchise` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_of_submition` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(255) NOT NULL,
  `landline` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `profession` varchar(255) NOT NULL,
  `premises` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `owned_leased` varchar(255) NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `total_area` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `headline` varchar(255) NOT NULL,
  `size` int(255) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`id`, `name`, `headline`, `size`, `date`) VALUES
(29, '895748323welcome to our  new website.pdf', 'Welcome To Our New Website . ', 977337, '2019-09-24 23:18:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admission`
--
ALTER TABLE `admission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`email`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `id_2` (`id`);

--
-- Indexes for table `franchise`
--
ALTER TABLE `franchise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admission`
--
ALTER TABLE `admission`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `franchise`
--
ALTER TABLE `franchise`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php include 'filesLogic.php';?>


<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>



<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="Sky-high-global-school" />
<meta name="keywords" content="kindergarten,children,kidsschool,school,baby,childschool,academy,course,education," />
<meta name="author" content="ThemeMascot" />

<!-- Page Title -->
<title>Sky-high-global-school</title>

<!-- Favicon and Touch Icons -->
<link href="images/favicon.png" rel="shortcut icon" type="image/png">
<link href="images/apple-touch-icon.png" rel="apple-touch-icon">
<link href="images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
<link href="images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
<link href="images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="css/menuzord-skins/menuzord-rounded-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<!-- CSS | Custom Margin Padding Collection -->
<link href="css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- Revolution Slider 5.x CSS settings -->
<link  href="js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link  href="js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
<link  href="js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>

<!-- CSS | Theme Color -->
<link href="css/colors/theme-skin-color-set-1.css" rel="stylesheet" type="text/css">

<!-- external javascripts -->
<script src="js/jquery-2.2.4.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="js/jquery-plugin-collection.js"></script>

<!-- Revolution Slider 5.x SCRIPTS -->
<script src="js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->





</head>
<body class="">
<div id="wrapper" class="clearfix">

  
  <!-- Header -->
  <header id="header" class="header">
    <div class="header-top bg-theme-color-sky sm-text-center p-0">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="widget no-border m-0">
              <ul class="list-inline font-13 sm-text-center mt-5">
                <li>
                  <a class="text-white" href="faqs.html">FAQ</a>
                </li>
                <li class="text-white">|</li>
                <li>
                  <a class="text-white" href="contact-us.html">Help Desk</a>
                </li>
                <li class="text-white">|</li>
                <li>
                  <a class="text-white" href="insider/index.php">Login</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-8">
            <div class="widget m-0 pull-right sm-pull-none sm-text-center">
              <ul class="list-inline pull-right">
                <li class="mb-0 pb-0">
                  <div class="top-dropdown-outer pt-5 pb-10">
                    <a class="top-cart-link has-dropdown text-white text-hover-theme-colored"></a>
                    <ul class="dropdown">
                      <li>
                        <!-- dropdown cart -->
                        <div class="dropdown-cart">
                          <table class="table cart-table-list table-responsive">
                            
                          </table>
                          <div class="total-cart text-right">
                            <table class="table table-responsive">
                              
                            </table>
                          </div>
                         
                        </div>
                        <!-- dropdown cart ends -->
                      </li>
                    </ul>
                  </div>
                </li>
               
              </ul>
            </div>
            <div class="widget no-border m-0 mr-15 pull-right flip sm-pull-none sm-text-center">
              <ul class="styled-icons icon-circled icon-sm pull-right flip sm-pull-none sm-text-center mt-sm-15">
                <li><a href="https://www.facebook.com/skyhighglobalschool"><i class="fa fa-facebook text-white"></i></a></li>
              
               
                <li><a href="https://www.instagram.com/skyhighglobalbbsr/"><i class="fa fa-instagram text-white"></i></a></li>
                
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-middle p-0 bg-lightest xs-text-center">
      <div class="container pt-0 pb-0">
        <div class="row">
          <div class="col-xs-12 col-sm-4 col-md-5">
            <div class="widget no-border m-0">
              <a class="menuzord-brand pull-left flip xs-pull-center mb-15" href="javascript:void(0)"><img src="image/logo.png" alt=""></a>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
              <ul class="list-inline">
                <li><i class="fa fa-phone-square text-theme-color-sky font-36 mt-5 sm-display-block"></i></li>
                <li>
                  <a href="#" class="font-12 text-black text-uppercase">Call us today!</a>
                  <h5 class="font-14 text-theme-color-blue m-0"> +91 7008776394</h5>
                </li>
              </ul>
            </div>
          </div>  
          <div class="col-xs-12 col-sm-4 col-md-3">
            <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
              <ul class="list-inline">
                <li><i class="fa fa-clock-o text-theme-color-red font-36 mt-5 sm-display-block"></i></li>
                <li>
                  <a href="#" class="font-12 text-black text-uppercase">We are open!</a>
                  <h5 class="font-13 text-theme-color-blue m-0"> Mon-Fri</h5>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper navbar-scrolltofixed bg-theme-color-red border-bottom-theme-color-sky-2px">
        <div class="container">
          <nav id="menuzord" class="menuzord bg-theme-colored pull-left flip menuzord-responsive">
            <ul class="menuzord-menu">
              <li><a href="index.php">Home</a>
                
              </li>
			    <li><a href="about.html">About Us</a>
                
              </li>
             
			  
			     <li><a href="#home">Methodology</a>
                <ul class="dropdown">
                  <li><a href="curriculum.html">Curriculum</a></li>
                  <li><a href="true-smart-learning-systems.html">True Smart Learning System </a></li>
                  <li><a href="facilities.html">Facilities </a></li>
                </ul>
              </li>
			  	     <li><a href="#home">Programs</a>
                <ul class="dropdown">
                  <li><a href="turbo-toddler-program.html">Turbo Toddler (Play Group)</a></li>
                  <li><a href="nursery-school-program.html"> Jumbo Nursery(Nursery)</a></li>
                  <li><a href="lower-kg-school-program.html">Dumbo junior (LKG) </a></li>
				  
                  <li><a href="upper-kg-program.html">Tembo senior (UKG)</a></li>
               
                </ul>
              </li>
			  
			    <li><a href="day-care.html">Day Care</a>
                
              </li>
             
              <li><a href="#home">Franchise</a>
                <ul class="dropdown">
                  <li><a href="what-you-need.html">What You Need </a></li>
                  <li><a href="shgs-support.html">SHGS Support </a></li>
                  <li><a href="how-to-start.html">How To Start </a></li>
				   <li><a href="application-form.html">Application Form </a></li>
                </ul>
              </li>
			   <li><a href="gallery.html">Gallery</a>
                
              </li>  
                    <li class="active"><a href="notice.php">Notice</a>
                
              </li>
               <li><a href="contact-us.html">Contact Us</a>
                
              </li>     
            <ul class="pull-right flip hidden-sm hidden-xs">
              <li>
                <!-- Modal: Book Now Starts -->
                <a class="btn btn-colored btn-flat bg-theme-color-sky text-white font-14 bs-modal-ajax-load mt-0 p-25 pr-15 pl-15" href="admission.html"><b>Admission Inquiry</b></a>
                <!-- Modal: Book Now End -->
              </li>
            </ul>
          
          </nav>
        </div>
      </div>
    </div>
  </header>
  
  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: home -->
    <section id="home">
      <div class="container-fluid p-0">
	  
        <div class="col-lg-12 col-md-12 col-sm-12">
        
              
		  <style>
		 hr{
  border: 1px solid black;
}
		  </style>
          <!-- end .rev_slider -->
		  <div class="col-lg-12 col-md-12 col-sm-12"
		  <div class="tg-noticeboardarea" style="background-color: #dcdee0;   box-shadow: 0 5px 25px 0 rgba(0,56,160,.15);border-width: thick;">

							<div class="tg-widget tg-widgetadmissionform">
								<div class="tg-widgetcontent">
									<h3 class="text-theme-color-sky" align="center">Notice Board</h3>
									<hr> </hr>
								

									<div class="tg-description" style="color:blue;">
									<?php foreach (array_reverse($files) as $file): ?>
									<p><?php echo $file['headline']; ?></p>
							
									<a href="insider/uploads/<?php echo $file['name'] ?>"><span style="color:red;"><i class="fa fa-file-pdf-o"></i>Download</span></a><hr></hr>
						
									<?php endforeach;?>
							
									
									
								
									</div>
								
								</div>
							</div>
						
							</div>
							</div>
        </div>
        <!-- end .rev_slider_wrapper -->
        <script>
          $(document).ready(function(e) {
            $(".rev_slider_default").revolution({
              sliderType:"standard",
              sliderLayout: "auto",
              dottedOverlay: "none",
              delay: 5000,
              navigation: {
                  keyboardNavigation: "off",
                  keyboard_direction: "horizontal",
                  mouseScrollNavigation: "off",
                  onHoverStop: "off",
                  touch: {
                      touchenabled: "on",
                      swipe_threshold: 75,
                      swipe_min_touches: 1,
                      swipe_direction: "horizontal",
                      drag_block_vertical: false
                  },
                arrows: {
                  style: "gyges",
                  enable: true,
                  hide_onmobile: false,
                  hide_onleave: true,
                  hide_delay: 200,
                  hide_delay_mobile: 1200,
                  tmp: '',
                  left: {
                      h_align: "left",
                      v_align: "center",
                      h_offset: 0,
                      v_offset: 0
                  },
                  right: {
                      h_align: "right",
                      v_align: "center",
                      h_offset: 0,
                      v_offset: 0
                  }
                },
                bullets: {
                  enable: true,
                  hide_onmobile: true,
                  hide_under: 800,
                  style: "hebe",
                  hide_onleave: false,
                  direction: "horizontal",
                  h_align: "center",
                  v_align: "bottom",
                  h_offset: 0,
                  v_offset: 30,
                  space: 5,
                  tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title"></span>'
                }
              },
              responsiveLevels: [1240, 1024, 778],
              visibilityLevels: [1240, 1024, 778],
              gridwidth: [1170, 1024, 778, 480],
              gridheight: [640, 768, 960, 720],
              lazyType: "none",
              parallax: {
                  origo: "slidercenter",
                  speed: 1000,
                  levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
                  type: "scroll"
              },
              shadow: 0,
              spinner: "off",
              stopLoop: "on",
              stopAfterLoops: 0,
              stopAtSlide: -1,
              shuffle: "off",
              autoHeight: "off",
              fullScreenAutoWidth: "off",
              fullScreenAlignForce: "off",
              fullScreenOffsetContainer: "",
              fullScreenOffset: "0",
              hideThumbsOnMobile: "off",
              hideSliderAtLimit: 0,
              hideCaptionAtLimit: 0,
              hideAllCaptionAtLilmit: 0,
              debugMode: false,
              fallbacks: {
                  simplifyAll: "off",
                  nextSlideOnWindowFocus: "off",
                  disableFocusListener: false,
              }
            });
          });
        </script>
		
		<style>
		.tg-noticeboardarea{
	width: 25%;
	float: right;
	padding: 0 0 0 20px;

	
	
}
.tg-noticeboardarea .tg-widget:first-child .tg-widgetcontent{background: #52b554;}
.tg-noticeboardarea .tg-widget + .tg-widget{margin: 21px 0 0;}
.tg-noticeboardarea .tg-widget .tg-widgetcontent{padding: 20px;}
		</style>
        <!-- Slider Revolution Ends -->
					  </div>
					  </div>
					  </div>
					  </div>
    </section>

    

    
   

   


   


  <!-- Footer -->
  <footer id="footer" class="footer divider layer-overlay overlay-dark-9" data-bg-img="images/bg/bg8.jpg">
    <div class="container">
      <div class="row border-bottom">
        <div class="col-sm-9 col-md-4">
          <div class="widget dark">
            <img class="mt-5 mb-20" alt="" src="image/logo2.png">
            <p>Plot No. LP-899,

Ground Floor,

Near Hanuman Temple, Prashanti Vihar,

Patia, Bhubaneshwar,

Odisha 751024</p>
            <ul class="list-inline mt-5">
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-theme-color-red mr-5"></i> <a class="text-gray" href="#">  +91 7008776394</a> </li>
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-theme-color-lemon mr-5"></i> <a class="text-gray" href="mailto: mail@shgschools.com">mail@shgschools.com</a> </li>
              
			  </ul>
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="widget dark">
            <h4 class="widget-title">Useful Links</h4>
            <ul class="list angle-double-right list-border">
              <li><a href="index.php">Home</a></li>
			  <li><a href="about.html">About Us</a></li>
			  <li><a href="gallery.html">Gallery</a></li>
              <li><a href="contact-us.html">Contact Us</a></li>
              <li><a href="faqs.html">Faqs</a></li>
             
			  <li><a href="shgs-support.html">SHGS Support</a></li>
			  <li><a href="application-form.html">Application Form</a></li>
                            
            </ul>
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="widget dark">
            <h4 class="widget-title">Facebook feed</h4>
            
			<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fskyhighglobalschool&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1807552176029053" width="400" height="500" style="border:none;overflow:hidden" scrolling="no" 
			frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

			
			</div>
          </div>
        </div>
     
      </div>
      <div class="row mt-30">
        <div class="col-md-9">
          <div class="widget dark" style="margin-left:100px;">
            <h5 class="widget-title mb-10">Call Us Now</h5>
            <div class="text-white">
             +91 7008776394
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="widget dark" style="margin-left:100px;">
            <h5 class="widget-title mb-10">Connect With Us</h5>
            <ul class="styled-icons icon-bordered icon-sm">
              <li><a href="https://www.facebook.com/skyhighglobalschool/"><i class="fa fa-facebook"></i></a></li>
        
            
            
              <li><a href="https://www.instagram.com/skyhighglobalbbsr/"><i class="fa fa-instagram"></i></a></li>
           
            </ul>
          </div>
        </div>
        
      </div>
    </div>
    <div class="footer-bottom bg-black-333">
      <div class="container pt-20 pb-20">
        <div class="row">
          <div class="col-md-6">
            <p class="font-12" style="color:white;">&copy; Sky High Global School,2019 | All Rights Reserved &nbsp; &nbsp; Developed By <a href="https://wiselab.in" style="color:#00BCD4;">Wiselab technologies.</a> </p>
			
		  </div>
		  
		  
		  <div  class="col-md-6" >
		
		  <p style="color:white;">Total Visitor: <img src='https://www.counter12.com/img-zzwAW2BaA993AxaW-26.gif' border='0' alt='counter'></p> 
		  
		  <script type='text/javascript' src='https://www.counter12.com/ad.js?id=zzwAW2BaA993AxaW'>
		  </script>
		  </div>
		
			
        </div>
      </div>
    </div>
  </footer>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="js/custom.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.video.min.js"></script>

</body>

<!-- Mirrored from html.kodesolution.live/s/kidspro/v2.1/demo/index-mp-layout1.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 06 Jul 2019 13:04:40 GMT -->
</html>